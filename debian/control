Source: pairtools
Section: python
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Antoni Villalonga <antoni@friki.cat>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all-dev,
               python3-all-dbg,
               cython3,
               cython3-dbg,
               python3-setuptools,
               python3-click,
               python3-numpy,
               python3-sphinx-click,
Standards-Version: 4.5.0
Homepage: https://github.com/mirnylab/pairtools
Vcs-Browser: https://salsa.debian.org/med-team/pairtools
Vcs-Git: https://salsa.debian.org/med-team/pairtools.git
Rules-Requires-Root: no

Package: python3-pairtools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
Description: Framework to process sequencing data from a Hi-C experiment
 Simple and fast command-line framework to process sequencing data from a Hi-C
 experiment.
 .
 Process pair-end sequence alignments and perform the following operations:
 .
   - Detect ligation junctions (a.k.a. Hi-C pairs) in aligned paired-end
     sequences of Hi-C DNA molecules
   - Sort .pairs files for downstream analyses
   - Detect, tag and remove PCR/optical duplicates
   - Generate extensive statistics of Hi-C datasets
   - Select Hi-C pairs given flexibly defined criteria
   - Restore .sam alignments from Hi-C pairs

Package: python3-pairtools-dbg
Architecture: any
Section: debug
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-pairtools (= ${binary:Version})
Description: Process sequencing data from a Hi-C experiment (debug build)
 Debug files for python3-pairtools, a simple and fast command-line framework to
 process sequencing data from a Hi-C experiment.
 .
 Process pair-end sequence alignments and perform the following operations:
 .
   - Detect ligation junctions (a.k.a. Hi-C pairs) in aligned paired-end
     sequences of Hi-C DNA molecules
   - Sort .pairs files for downstream analyses
   - Detect, tag and remove PCR/optical duplicates
   - Generate extensive statistics of Hi-C datasets
   - Select Hi-C pairs given flexibly defined criteria
   - Restore .sam alignments from Hi-C pairs

Package: python3-pairtools-examples
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
Enhances: python3-pairtools
Description: Process sequencing data from a Hi-C experiment (examples)
 Simple and fast command-line framework to process sequencing data from a Hi-C
 experiment.
 .
 Process pair-end sequence alignments and perform the following operations:
 .
   - Detect ligation junctions (a.k.a. Hi-C pairs) in aligned paired-end
     sequences of Hi-C DNA molecules
   - Sort .pairs files for downstream analyses
   - Detect, tag and remove PCR/optical duplicates
   - Generate extensive statistics of Hi-C datasets
   - Select Hi-C pairs given flexibly defined criteria
   - Restore .sam alignments from Hi-C pairs
 .
 This package contains some examples
